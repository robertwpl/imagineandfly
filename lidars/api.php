<?php
function read($name,$def='[]'){
	if (file_exists($name))
		return file_get_contents($name);
	else
		return $def;
}
function saveWithCopy($name,$data){
	if(!is_file($name)){
		file_put_contents($name, $data);
		return 'ok';
	}
	$copyIdx=1;
	$base=substr($name,0,strpos($name,"."));
	$list=array();
	foreach(new DirectoryIterator('copies') as $info){
		if(strpos($info->getBasename(),$base)===0){
			$list[]=array($info->getMTime(),$info->getFilename());
		}
	}
	if(count($list)>=100){
		usort($list,function($a,$b){
			if($a[0]==$b[0]){
				return 0;
			}
			return $a[0]>$b[0]?1:-1;
		});
		unlink('copies/'.$list[0][1]);
	}
	$nName='copies/'.str_replace('.',"-".date("Y-m-d--H-i-s").".",$name);
	if(copy($name,$nName)){
		file_put_contents($name, $data);
		return 'ok';
	}
	return 'Not saved';
}
function upload(){
	$target_dir = "copies/";
	$target_file = $target_dir . basename($_FILES["file"]["name"]);
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	$check = getimagesize($_FILES["file"]["tmp_name"]);
	if($check !== false) {
		move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
		return "Uploaded ".$_FILES["file"]["name"];
	} else {
		return "File is not an image.";
	}
}
if ($_POST['action'] == 'read')
	echo read('data.json');
else if ($_POST['action'] == 'save')
	echo saveWithCopy('data.json', $_POST['data']);
else if ($_POST['action'] == 'read_markers')
	echo read('markers.json');
else if ($_POST['action'] == 'save_markers')
	echo saveWithCopy('markers.json', $_POST['markers']);
else if ($_POST['action'] == 'read_maps')
	echo read('maps.json');
else if ($_POST['action'] == 'save_maps')
	echo saveWithCopy('maps.json', $_POST['maps']);
else if ($_POST['action'] == 'upload')
	echo upload();
