this.exportBitmapMap= (map)->
	class BitmapMap extends google.maps.OverlayView

		constructor:(@map)->
			@setMap(@map)
			@created=false
			@bmProperties={scale:1,alpha:1}
			@dragPosition=[]
			@canvasPosition=[0,0]
			@dragging=false
			@draggingOn=false
			@mapDragTo=[0,0]
			@dragPosition2=[0,0]

		uiCreate:->
			$outer=closerDeco.closerGet('Bitmap')

			$scale=$('<input>')
			.attr({title:'skala',type:'range',min:0,max:1,value:1,step:.01})
			.on('input',=>
				@bmProperties.scale=$scale.val()
				@redraw()
				@save()
			)
			.addClass('line')
			.appendTo($outer)

			$alpha=$('<input>').attr({title:'przezroczystość',type:'range',min:0,max:1,value:1,step:.1})
			.on('input',=>
				@bmProperties.alpha=$alpha.val()
				@redraw()
				@save()
			)
			.addClass('line')
			.appendTo($outer)

			$dragDiv=$('<div>')
			.addClass('line')
			.appendTo($outer)

			numSetup= ($input)->
				$input.attr
					type:'number'
					step:'0.001'
			$lat=$('<input title="lat">')
			.change(=>
				console.log @
			)
			.appendTo($dragDiv)
			numSetup($lat)

			$lng=$('<input title="lng">')
			.change(=>
				console.log @
			)
			.appendTo($dragDiv)
			numSetup($lng)

			$input=$('<input type="checkbox" id="dragDisable">')
			.change(=>
				@map.setOptions({draggable:!$input.prop('checked')})
				@draggingOn=$input.prop('checked')
			)
			.appendTo($dragDiv)
			$('<label>')
			.attr({'for':'dragDisable'})
			.text('przesuwanie')
			.appendTo($dragDiv)

			@$bitmapPath=$('<input type="text">')
			.addClass('line')
			.appendTo($outer)
			.on('input',=>
				@dragPosition2=[0,0]
				@save())
			$form=$('<form>').attr
													method:'post'
													enctype:'multipart/form-data'
						.appendTo($dragDiv)
			$('<input>').attr
										type:'hidden'
										name:'action'
										value:'upload'
						.appendTo($form)
			$('<input>').attr
										type:'file'
										name:'file'
						.appendTo($form)
			$('<input>').attr
										type:'submit'
										value:'upload'
						.appendTo($form)
						.click(->
							$.ajax(
								url:'api.php'
								type:'post'
								data:new FormData($form[0])
								cache: false
								contentType: false
								processData: false
							)
							.fail((e)->
								console.log e
								alert 'Upload failed'
							)
							.success((e)->
								console.log e
							)
							return false
						)


		append:(url)->
			@img=document.createElement('@img')
			@img.addEventListener('load',=>@redraw())
			@img.src=url
			@$bitmapPath.val(url)
		onAdd:=>
			if @created
				return
			@created=true
			@$canvas=$('<canvas>').attr(
				width:$(document).width()
				height:$(document).height()
			)
			.css(
				position:'absolute'
			)
			.appendTo($(@getPanes().mapPane))
			@ctx=@$canvas.get()[0].getContext('2d')
			@uiCreate()
			@map.addListener('mousedown',(e)=>
				if @draggingOn
					@dragging=true
					@dragPosition=[e.pixel.x,e.pixel.y]
			)
			@map.addListener('mousemove',(e)=>
				if @dragging
					@redraw([e.pixel.x,e.pixel.y])
			)
			@map.addListener('mouseup',(e)=>
				if @dragging
					@dragPosition2=[e.pixel.x-@dragPosition[0]+@canvasPosition[0],e.pixel.y-@dragPosition[1]+@canvasPosition[1]]
					@dragging=false
					@save()
			)
			@map.addListener('zoom_changed',=>
				@redraw()
			)
			#			of map, without moving bitmap
			@map.addListener('dragend',=>
				proj=@getProjection()
				sw=proj.fromLatLngToDivPixel(@map.getBounds().getSouthWest())
				ne=proj.fromLatLngToDivPixel(@map.getBounds().getNorthEast())
				@mapDragTo=[sw.x,ne.y]
				@save()
			)
			@read()

		redraw:->
			if @img.src==undefined
				return
			if arguments.length==1
				@$canvas.css({left:arguments[0][0]-@dragPosition[0]+@canvasPosition[0]-@mapDragTo[0],top:arguments[0][1]-@dragPosition[1]+@canvasPosition[1]-@mapDragTo[1]})
			@ctx.clearRect(0,0,@$canvas.width(),@$canvas.height())
			@$canvas.width($(document).width()-@canvasPosition[0])
			@$canvas.height($(document).height()-@canvasPosition[1])
			@ctx.globalAlpha=@bmProperties.alpha
			scale=@bmProperties.scale*@map.getZoom()/23
			@ctx.drawImage(@img,0,0,@$canvas.width()/scale,@$canvas.height()/scale,0,0,@img.width,@img.height)

		draw:->

		read:=>
			$.post('api.php',
				action:'read_maps',
				null,
				'json')
			.done((data)=>
				@maps=data
				###@append(data.url)
				@bmProperties.scale=data.scale
				@bmProperties.alpha=data.alpha
				point=Convert.latLng2Point(@map,data.position)
				@canvasPosition=[point.x,point.y]
				console.log '@canvasPosition', @canvasPosition###
			)

		save:->
			if @$bitmapPath.val()==''
				return
			@maps[0]=
					url:@$bitmapPath.val()
					scale:@bmProperties.scale
					alpha:@bmProperties.alpha
					position:Convert.xy2LatLng(@map,@dragPosition2)
			$.post('api.php',
				{action:'save_maps',maps:JSON.stringify(@maps)})
			.fail(->
				alert 'failed to save bitmap parameters'
			)
	new BitmapMap(map)