window.initMap = ->
	pins=null
	class WmsOverlay extends google.maps.OverlayView
		[lidarOn,alpha]=[false,1]
		constructor:(@url,@map) ->
			@setMap(@map)
			@map.addListener(
				'dragend',=>
					@loadTile()
			)
			@map.addListener(
				'zoom_changed',=>
					@loadTile()
			)
			hashWatch.mapWatch(@map)
			hashWatch.on('zoom',(zoom)=>
				map.setZoom(parseInt(zoom))
			)
			hashWatch.on('lat lng',(lat,lng)=>
				map.setCenter(new google.maps.LatLng({lat:parseFloat(lat),lng:parseFloat(lng)}))
			)
			hashWatch.on('lidarOn lidarAlpha',(p1, p2)=>
				load=lidarOn!=p1
				draw=alpha!=parseFloat(p2)
				lidarOn=p1
				alpha=parseFloat(p2)
				if load
					@loadTile()
				else if draw
					@drawImage()
			)
			hashWatch.on('maptypeid',(type)=>
				map.setMapTypeId(type)
			)
			@created=false

		resizeLayout:=>
			@canvas.setAttribute('width',$(document).width())
			@canvas.setAttribute('height',$(document).height())
			@loadTile()

		onAdd:=>
			if @created
				return
			@created=true
			@canvas=document.createElement('canvas')
			@canvas.style.position='absolute'
			@ctx=@canvas.getContext('2d')
			@getPanes().mapPane.appendChild(@canvas)
			$outer=closerDeco.closerGet('Lidar')
			$('<div>')
			.text('home')
			.addClass('gog-btn2')
			.css('marginBottom','5px')
			.appendTo($outer)
			.click(->
				document.location='/lidars/'
			)
			$btn=$('<div>').addClass('gog-btn2').text('Lidar').appendTo($outer).click(=>
				lidarOn=!lidarOn
				$btn.css('backgroundColor',if lidarOn then '#4640ff' else '#ffffff')
				$(@canvas).toggle(lidarOn)
				hashWatch.update({lidarOn:lidarOn})
			)
			$range=$('<input>').attr({type:'range',min:0,max:1,value:1,step:.1})
			.appendTo($outer)
			.on('input',=>
				alpha=$range.val()
				@drawImage()
			)
			.on('change',=>
				hashWatch.update({lidarAlpha:alpha})
			)
			.val(alpha)
			@$indicator=$('<img>').addClass('indicator').toggleClass('hidden').appendTo($outer)


			@image=document.createElement('img')
			@image.addEventListener('load',=>
				@$indicator.toggleClass('hidden')
				@drawImage()
			)
			@resizeLayout()
			$(window).on('resize',@resizeLayout)


		draw:->

		loadTile:->
			if !lidarOn
				return
			ba=Convert.googleBounds2Bbox(@map.getBounds())
			url = @url + '?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&LAYERS=' + 'NMT_100,NMT_30,NMT_5' + '&CRS=' + 'EPSG:4326' + '&FORMAT=image/png8&WIDTH=' + $(document).width() + '&HEIGHT=' + $(document).height() + '&BBOX=' + ba.join(',')
			if @image.src!='' && @drawZoom!=@map.getZoom()
				@ctx.clearRect(0,0,@canvas.width,@canvas.height)
				@ctx.drawImage(@image,0,0,@canvas.width*@map.getZoom()/@drawZoom,@canvas.height*@map.getZoom()/@drawZoom)
			@image.src=url
			@$indicator.toggleClass('hidden')

		drawImage:->
			if !lidarOn
				return
			@drawZoom=@map.getZoom()
			@ctx.globalAlpha=alpha
			proj=@getProjection()
			sw=proj.fromLatLngToDivPixel(@map.getBounds().getSouthWest())
			ne=proj.fromLatLngToDivPixel(@map.getBounds().getNorthEast())
			$(@canvas).css({left:sw.x,top:ne.y})
			@ctx.clearRect(0,0,@canvas.width,@canvas.height)
			@ctx.drawImage(@image,0,0)

	class Pins extends google.maps.OverlayView
		[markers,run,$dragMarkers]=[[],false,null]
		constructor:(@map)->
			@markerCluster = new MarkerClusterer(@map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'})
			@setMap(@map)

		changeState:->
			run=!run
			if run
				@$btn.addClass('active')
			else
				@$btn.removeClass('active')

		onAdd:=>
			$outer=closerDeco.closerGet('Pin')
			@$btn=$('<div>').addClass('rectBtn').css(
				backgroundImage:'url(pin.png)'
			)
			.click((e)=>
				@changeState()
			)
			.appendTo($outer)
			$('<div>').addClass('rectBtn').css(
				backgroundImage:'url(export.png)'
			)
			.click((e)=>
				@exportKml()
			)
			.appendTo($outer)
			$dragMarkers=$('<input type="checkbox" id="dragMarkers">')
			.change((e)=>
				checked=$(e.target).prop('checked')
				markers.forEach((m,i)=>
					m.setDraggable(checked))
			)
			.appendTo($outer)
			$('<label>')
			.attr({'for':'dragMarkers'})
			.text('drag markers')
			.appendTo($outer)
			@map.addListener('click',(e)=>
				if !run
					return
				@markerCreate(e.latLng,'',true)
				@save()
			)
			@read()

		updateMarker:(marker,title)=>
			marker.setTitle(title)
			marker.setLabel(title.substr(0,1))

		deleteMarker:(marker,win)=>
			@markerCluster.removeMarker(marker)
			marker.setMap(null)
			markers.forEach((e,i)=>
				if e.id==marker.id
					markers.splice(i,1)
			)
			@save()

		markerCreate:(latLng,title,show)=>
			marker=new google.maps.Marker(
				position:latLng
				map:@map
				draggable:$dragMarkers.prop('checked')
			)
			@markerCluster.addMarker(marker)
			@updateMarker(marker,title)
			marker.id=new Date().getTime()+''+markers.length
			markers.push(marker)
			win=null
			dblclick=()=>
				$all=$('<div>')
				$content=$('<input type="text" value="'+marker.title+'">')
				.on('change',(e)=>
					@updateMarker(marker,$content.val())
					@save()
				)
				.on('keypress',(e)=>
					if e.which==13
						win.close()
				)
				.appendTo($all)
				$('<div>').css(
					width:16
					height:16
					background:'url(bin.png)'
					margin:'5px'
					float:'right'
				)
				.on('click',(e)=>
					@deleteMarker(marker)
					win.close()
				)
				.appendTo($all)
				win=new google.maps.InfoWindow(
					content:$all.get(0)
				)
				win.open(@map,marker)
			marker.addListener('dblclick',=>
				dblclick()
			)
			marker.addListener('dragend',=>
				@save()
			)
			if show==true
				dblclick()

		draw:->
		read:=>
			$.post('api.php',
						action:'read_markers',
						null,
						'json')
			.done((data)=>
				data.forEach((e,i)=>
					@markerCreate(new google.maps.LatLng({lat:e[0],lng:e[1]}),e[2])
				)
			)

		save:=>
			m=[]
			markers.forEach((e,i)=>
				m.push([e.getPosition().lat(),e.getPosition().lng(),e.getTitle()])
			)
			$.post('api.php',
				{action:'save_markers',markers:JSON.stringify(m)})
			.fail(->
				alert 'failed'
			)

		exportKml:=>
			x="<?xml version='1.0' encoding='UTF-8'?>"
			x+="<kml xmlns='http://www.opengis.net/kml/2.2'>"
			x+="<Document>"
			x+="<name>"
			d=new Date
			name="Pomysły odkrywcy "+d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes()
			x+=name
			x+="</name>"
			markers.forEach((e)=>
				x+="<Placemark>"
				x+="<name>"
				x+=e.getTitle()
				x+="</name>"
				x+="<Point>"
				x+="<coordinates>"
				x+=e.getPosition().lng()+','
				x+=e.getPosition().lat()+','
				x+='0.0'
				x+="</coordinates>"
				x+="</Point>"
				x+="</Placemark>"
			)
			x+="</Document>"
			x+="</kml>"
			saveAs(new Blob([x],{type:'application/xhtml+xml;charset=utf-8'}),name+'.kml')

	class Search extends google.maps.OverlayView
		[created,timeLastCheck,intervalId,textSaved,textRepeat,$results,$input,$sign,zoomLast]=[false,0,0,'','',null,null,null,undefined]
		constructor:(@map)->
			@setMap(@map)
			hashWatch.on('search',(text)=>
				if $input?
					$input.val(text)
				@query(text,Number.MAX_VALUE)
			)
			@map.addListener(
				'dragend',(e)=>
					@queryRepeat()
			)
			@map.addListener(
				'zoom_changed',(e)=>
					if zoomLast && zoomLast!=@map.getZoom()
						zoomLast=@map.getZoom()
						@queryRepeat()
			)
		onAdd:=>
			if created
				return
			created=true
			@uiCreate()
		draw:->

		queryOSM:(text)=>
			if text==''
				return
			b=Convert.googleBounds2OSM(@map.getBounds())
			pb=new PathBuilder
			$results.hide()
			$sign.show()
			$.get('http://nominatim.openstreetmap.org/search/'+text+'?'+pb.asGetParams(
				format:'json'
				limit:100
				viewbox:b.join(',')
				bounded:1
			))
			.done((data)=>
				if !$.isArray(data)
					data=[]
					alert('Błąd w wynikach szukania')
				data=(item for item in data when item.type=='place' || item.type=='administrative' || item.type=='village'|| item.type=='information'  || item.type=='river')
				$sign.hide()
				$results.find('*').remove()
				if data.length>0
					$results.show()
				[marker]=[null]
				for k,v of data
					$('<li>')
						.html([v.display_name,v.class,v.type].join('<br>'))
						.on('click',=>
							z=@map.getZoom()
							@map.setZoom(14)
							latLng=new google.maps.LatLng({lat:parseFloat(v.lat),lng:parseFloat(v.lon)})
							@map.setCenter latLng
							$results.hide()
							pins.markerCreate(latLng,v.display_name,true)
						)
						.on('mouseover',=>
							if marker
								marker.setMap(null)
							marker=new google.maps.Marker(
								position:new google.maps.LatLng({lat:parseFloat(v.lat),lng:parseFloat(v.lon)})
								map:@map
								icon:'point.png'
							)
						)
						.on('mouseout',=>
							marker.setMap(null)
						)
						.appendTo($results)
			)
		queryRepeat:=>
			textSaved=''
			@query(textRepeat,Number.MAX_VALUE)

		query:(text,timeDelta)=>
			hashWatch.update({search:text})
			if text==''
				$results.hide()
			if textSaved!=text && timeDelta>2000
				textRepeat=textSaved=text
				@queryOSM(text)

		uiCreate:->
			$outer=closerDeco.closerGet('Search')
			$input=$('<input type="search">')
				.on('focus',=>
					timeLastCheck=0
					textSaved=''
					time=new Date().getTime()
					intervalId=setInterval(=>
						@query($input.val(),new Date().getTime()-time)
					,1000)
				)
				.on('search',(e)=>
					@query($input.val(),Number.MAX_VALUE)
				)
				.on('blur',=>
					@query($input.val(),Number.MAX_VALUE)
					clearInterval(intervalId)
				)
				.val(hashWatch.ge)
				.appendTo($outer)
			$results=$('<ol>').hide().addClass('results').appendTo($outer)
			$sign=$('<img>').attr({src:'star.gif'}).hide().appendTo($outer)

	start=(position)=>
		window.map=map=new google.maps.Map(
			document.getElementById('map'),
			center:
				lat:position.coords.latitude
				lng:position.coords.longitude
			zoom:7
			scaleControl:true
			streetViewControlOptions:
				position:google.maps.ControlPosition.LEFT_TOP
		)
		window.hashWatch=new HashWatch
		window.closerDeco=new CloserDeco(map)
		window.wms=new WmsOverlay('http://mapy.geoportal.gov.pl/wss/service/img/guest/CIEN/MapServer/WMSServer',map)
		pins=new Pins(map)
		new Search(map)
#		exportBitmapMap(map)
		setTimeout(=>
			closerDeco.initState()
			hashWatch.hashChange(true)
		,2000
		)
		console.log 'MovieMapping started'
	glOk=false
	try
		if location.indexOf('192')>-1 && navigator.geolocation
			navigator.geolocation.getCurrentPosition(start)
			glOk=true
	catch
	if !glOk
		start(
			coords:
				longitude:20.963641421661347
				latitude:51.44503555633341
		)


