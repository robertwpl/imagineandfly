class window.HashWatch
  constructor:->
    @active=true
    @clbks=[]
    @params={}
    $(window).bind('hashchange',=>
      @hashChange()
    )
    @hashToParams()

  hashToParams:->
    hash=location.hash.substr(2)
    if hash==''
      return
    hash=hash.split('|')
    for k,v of hash
      v=v.split(',')
      @params[v[0]]=v[1]

  hashChange:(force)->
    if !@active
      return
    hash=location.hash.substr(2)
    if hash==''
      return
    hash=hash.split('|')
    col={}
    for k,v of hash
      v=v.split(',')
      v[1]=@_unserialize(v[1])
      col[v[0]]=v[1]
    for k,v of col
      if(v!=@params[k] ||force)
        keys=@clbks[k][0]
        values=[]
        for i,v of keys
          if col[v]!=null
            values.push(col[v])
            col[v]=null
        if values.length>0
#          console.log 'HashWatch->hashChange(), applying', values
          @clbks[k][1].apply(@clbks[k][1],values)

  update:(params)->
#    console.log 'HashWatch->update()', params,@params
    for k,v of params
      v=@_serialize(v)
      @params[k]=v
    col=[]
    for k,v of @params
      col.push("#{k},#{v}")
    location.hash='#!'+col.join('|')

  on:(events,clb)->
    events=events.split(' ')
    for k,v of events
      @clbks[v]=[events,clb]

  mapWatch:(map)->
    map.addListener(
      'dragend',=>
        center=map.getCenter()
        @update({lat:center.lat(),lng:center.lng()})
    )
    map.addListener(
      'zoom_changed',=>
        @update({zoom:map.getZoom()})
    )
    map.addListener(
      'maptypeid_changed',=>
        @update({maptypeid:map.getMapTypeId()})
    )

  _serialize:(v)->
    if(v+''=='true' || v+''=='false')
      v=if v then 'on' else 'off'
    v
  _unserialize:(v)->
    if v=='on' || v=='off'
      v=if v=='on' then true else false
    v
