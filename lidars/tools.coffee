class window.PathBuilder
  asGetParams:(params)->
    ar=[]
    for k,v of params
      ar.push(k+'='+v)
    ar.join('&')

class window.Convert

  @googleBounds2Bbox:(bounds)->
    #    wms 1.3.0
    [bounds.getSouthWest().lat(),bounds.getSouthWest().lng(),bounds.getNorthEast().lat(),bounds.getNorthEast().lng()]

  @googleBounds2OSM:(bounds)->
    [bounds.getSouthWest().lng(),bounds.getSouthWest().lat(),bounds.getNorthEast().lng(),bounds.getNorthEast().lat()]

  @xy2LatLng:(map,xy)->
    ne=map.getBounds().getNorthEast()
    sw=map.getBounds().getSouthWest()
    topRight=map.getProjection().fromLatLngToPoint(ne)
    bottomLeft=map.getProjection().fromLatLngToPoint(sw)
    scale=1 << map.getZoom()
    latLng=map.getProjection().fromPointToLatLng(new google.maps.Point(xy[0]/scale+bottomLeft.x,xy[1]/scale+topRight.y))
    [latLng.lat(),latLng.lng()]

  @latLng2Point:(map,latLngPoints)->
    topRight=map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast())
    bottomLeft=map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest())
    scale=Math.pow(2,map.getZoom())
    latLng=new google.maps.LatLng(latLngPoints[0],latLngPoints[1])
    worldPoint=map.getProjection().fromLatLngToPoint(latLng)
    console.log 'latLng',latLng.lat(),latLng.lng()
    console.log 'worldPoint', worldPoint.x,worldPoint.y
    p2=new google.maps.Point((worldPoint.x-bottomLeft.x)*scale,(worldPoint.y-topRight.y)*scale)
    console.log 'fromCenter',Convert.equiRectangularDistance(latLng,map.getCenter())
    console.log 'distanceTest',Convert.equiRectangularDistance(new google.maps.LatLng(0,0),new google.maps.LatLng(0,1))
    worldPoint

  @equiRectangularDistance:(p1,p2)->
    x=(p2.lng()-p1.lng())*Math.cos(Math.PI/180*(p1.lat()+p2.lat())/2)
    y=p2.lat()-p1.lat()
    console.log x,y
    Math.sqrt(x*x+y*y)*6371000


class window.CloserDeco
  constructor:(@map)->
  [idx,$hidings]=[0,[]]
  closerGet:(label)=>
    idxMy=idx
    $outer=$('<div>').addClass('gm-style-mtc gm-own').css({float:'left'})
    $clicker=$('<a class="clicker" href="#">')
    .click(=>
      $hiding.toggleClass('hidden')
      cur=hashWatch.params.controls
      if cur
        cur=cur.split('-')
        cur[idxMy]=if $hiding.hasClass('hidden') then '0' else '1'
        cur=cur.join('-')
      hashWatch.update({controls:cur})
      false
    )
    .text(label.substr(0,1).toLowerCase())
    .attr({title:label})
    .appendTo($outer)
    $hiding=$('<div class="controls-container">')
    .appendTo($outer)
    $hidings.push($hiding)
    @map.controls[google.maps.ControlPosition.TOP_LEFT].push($outer.get()[0])
    idx++
    $hiding
  initState:=>
    hashWatch.on('controls',(str)=>
      all=str.split('-')
      for i,v of all
        if v=='1'
          $hidings[i].removeClass('hidden')
        else
          $hidings[i].addClass('hidden')
    )
    if !hashWatch.params.hasOwnProperty('controls')
      hashWatch.update({controls:(1 for [0..idx-1]).join('-')})


